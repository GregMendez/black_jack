<?php

// Initialisation des valeurs uniquement lors d'une nouvelle partie
// Gérer la sauvegarde de ces informations pendant la partie (A FAIRE)
session_start();
$player_cards = $croupier_cards = array();
$player_total = $croupier_total = 0;
$money = 20;

$_SESSION["player_cards"] = $player_cards;
$_SESSION["player_total"] = $player_total;
$_SESSION["money"] = $money;

// On défini les cartes possibles (52 cartes) dans un tableau
// Gérer la sauvegarde de ces informations pendant la partie (A FAIRE)
$deck = array();
$colors = array( 'heart', 'diam', 'club', 'spade' );

// Pour chaque couleur
foreach ( $colors as $color )
{
	$deck[] = array( 'value' => '2', 'color' => $color, 'points' => 2 );
	$deck[] = array( 'value' => '3', 'color' => $color, 'points' => 3 );
	$deck[] = array( 'value' => '4', 'color' => $color, 'points' => 4 );
	$deck[] = array( 'value' => '5', 'color' => $color, 'points' => 5 );
	$deck[] = array( 'value' => '6', 'color' => $color, 'points' => 6 );
	$deck[] = array( 'value' => '7', 'color' => $color, 'points' => 7 );
	$deck[] = array( 'value' => '8', 'color' => $color, 'points' => 8 );
	$deck[] = array( 'value' => '9', 'color' => $color, 'points' => 9 );
	$deck[] = array( 'value' => '10', 'color' => $color, 'points' => 10 );
	$deck[] = array( 'value' => 'Valet', 'color' => $color, 'points' => 10 );
	$deck[] = array( 'value' => 'Dame', 'color' => $color, 'points' => 10 );
	$deck[] = array( 'value' => 'Roi', 'color' => $color, 'points' => 10 );
	$deck[] = array( 'value' => 'As', 'color' => $color, 'points' => 11 );
}

$_SESSION["deck"] = $deck;
$_SESSION["colors"] = $colors;

// On défini l'étape en cours
$step = isset( $_REQUEST['step'] ) ? (int)$_REQUEST['step'] : 1;

// Gestion du déroulement de la partie
switch ( $step )
{
	// Tour d'initialisation
	case 2:
		// Récupération, contrôle et enregistrement de la mise (A FAIRE)
		//$bet = isset( $_REQUEST['bet'] ) ? (int)$_REQUEST['bet'] : 0;
		if (isset( $_REQUEST['bet'])) {
			if ($_REQUEST['bet'] == "all") {
				$bet = $money;
				$money = 0;
			}
			else {
				$bet = (int)$_REQUEST['bet'];
				$money -= $bet;
			}

		}
		else {
			$bet = 0;
		}


		$_SESSION['bet'] = $bet;
		$_SESSION['money'] = $money;

		// Tirage de la première carte du joueur
		$player_cards[] = draw_card( $deck );

		// Tirage de la seconde carte du joueur
		$player_cards[] = draw_card( $deck );

		// Calcul des points du joueur
		foreach ( $player_cards as $card )
			$player_total += $card['points'];

		// Tirage de la première carte du croupier
		$croupier_cards[] = draw_card( $deck );

		// Calcul des points du croupier
		foreach ( $croupier_cards as $card )
			$croupier_total += $card['points'];

		// Si le joueur à 21 points, il gagne automatiquement et
		// récupère immédiatement 1,5 fois sa mise (A FAIRE)
		if($player_total == 21) {
			$money += $bet * 1.5;
		}
		break;

	// Tour du joueur
	case 3:

		if(isset( $_GET['param'])) {
			// Le joueur souhaite doubler sa mise et il a les moyens de la faire (A FAIRE) DONE
			if($_GET['param'] == 'mise') {
				if($money >= $bet * 2) {
					$bet *= 2;
					$money -= $bet;
				}
			}

			// Le joueur demande une nouvelle carte (A FAIRE) DONE
			if($_REQUEST['param'] == "carte") {
				$player_cards[] = draw_card($deck);
			}

			if($_REQUEST['param'] == "passe") {
				$step = 4;
			}
		}
		// Si le joueur a 21 points, il gagne automatiquement et
		// récupère immédiatement 1,5 fois sa mise (A FAIRE)DONE
		if($player_total == 21) {
			$money = $bet * 1.5;
		}
		// Si le joueur a plus de 21 points, il perd sa mise (A FAIRE) DONE
		if($player_total > 21) {
			$bet = 0;
		}
		// Afficher le résultat de se tour (A FAIRE)

		break;

	// Tour du croupier
	case 4:

		while ($croupier_total < 17) {
			// Tant qu'il a moins de 17, il tire une carte (A FAIRE) DONE
			$croupier_cards[] = draw_card($deck);
		}

		if ($croupier_total == 17) {
			// S'il a 17 mais avec un As en main, il tire une carte (A FAIRE) TODO
		}
		else if ($croupier_total > 17 && $croupier_total < 21) {
			$step = 3;
		}
		else if ($croupier_total > 21) {
			// Si le croupier a plus de 21 points, le joueur
			// récupère immédiatement 1,5 fois sa mise (A FAIRE) DONE
			$money += $bet*1.5;
		}
		else if ($croupier_total == 21)
		{
			// Si le croupier a 21 points, le joueur perd sa mise (A FAIRE) DONE
			$bet = 0;
		}

		// Si le joueur a passé et que le croupier a passé,
		// le joueur récupère sa mise (A FAIRE)

		// Afficher le résultat de se tour (A FAIRE)

		// Après cette étape, soit on recommance l'étape 3 ou on recommence une partie (A FAIRE)

		break;
}


/**
 * Fonction de tirage de carte
 *
 * Note: le deck est passé en référence afin de conserver
 * 		 les modification en dehors de la fonction
 */
function draw_card( &$deck )
{
	// on tire au hasard un nombre entre 0 et la taille du deck-1
	$rand = rand( 0, count( $deck ) - 1 );
	$card = $deck[$rand];

	// On supprime la carte du deck
	unset( $deck[$rand] );

	// On réinitialise les index du tableau
	$deck = array_values( $deck );

	// On retourne la carte
	return $card;
}

?>
<html lang="fr" class="">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy" rel="stylesheet">

	<style type="text/css">
	* {
		-webkit-font-smoothing: antialiased;
	}

	body {
		font-family: 'helvetica neue';
		font-size: 16px;
		margin: 0;
		background: #147729; /* Old browsers */
		background: -moz-radial-gradient(center, ellipse cover, #147729 0%, #043604 100%); /* FF3.6-15 */
		background: -webkit-radial-gradient(center, ellipse cover, #147729 0%,#043604 100%); /* Chrome10-25,Safari5.1-6 */
		background: radial-gradient(ellipse at center, #147729 0%,#043604 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#147729', endColorstr='#043604',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
		color: white;
	}

	.wrapp{
		width: 700px;
		margin: 0 auto;
		padding-top: 50px;
		position: relative;
	}

	h1, h2{
		font-family: 'Luckiest Guy', cursive;
		font-size: 45px;
		font-weight: normal;
		color: white;
		text-align: center;
	}
	h2{ color: #033900; font-size: 25px; text-shadow: 0 0 5px #469C41 }

	form{
		text-align: center;
	}

	button[type="submit"]{
		display: block;
		margin: 10px auto;
		padding: 5px 20px;
		border: none;
		background: #033900;
		color: white;
		font-size: 18px;

	}

	#money{
		padding-left: 40px;
		position: absolute;
		right: 0; top: 20px;
	}

	#money svg{
		width: 30px;
		position: absolute;
		left: 0; top: 0;
	}

	#money strong{ color: #F8B317 }

	.player{
		text-align: center;
	}
	.player + .player{ margin-top: 30px; }

	.player h2 span{
		display: inline-block;
		width: 40px;
		padding: 6px 0 0;
		margin-left: 10px;
		background: #d40000;
		border-radius: 100px;
		color: white;
		text-shadow: 0 0 5px #450002;
	}

	.card{
		display: inline-block;
		width: 50px;
		height: 80px;
		padding: 3px;
		position: relative;
		border: 1px solid white;
		border-radius: 4px;
		background: white;
		text-align: left;
		box-shadow: 0px 0px 1px black;
		cursor: pointer;
		-webkit-transition: all .2s ease;
			-ms-transition: all .2s ease;
				transition: all .2s ease;
	}
	.card + .card{ margin-left: -25px }

	.card:hover{
		-webkit-transform: translateY(-20px);
			-ms-transform: translateY(-20px);
				transform: translateY(-20px);
	}

	.card:before{
		display: block;
		width: 100%; height: 100%;
		line-height: 100px;
		position: absolute;
		left: 0; top: 0;
		font-size: 40px;
		text-align: center;
	}

	.card-heart, .card-diam{ color: red }
	.card-spade, .card-club{ color: black }

	.card-heart:before{ content: '♥' }
	.card-diam:before{ content: '♦' }
	.card-spade:before{ content: '♠' }
	.card-club:before{ content: '♣' }

	.actions{ margin: 40px; text-align: center }
	.actions a{
		display: inline-block;
		padding: 5px 10px;
		border: none;
		background: #033900;
		color: white;
		font-size: 18px;
		text-decoration: none;
	}

	</style>

	<title>Black Jack</title>
</head>

<body>
	<div class="wrapp">
		<h1>Black Jack</h1>
		<div id="money">
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 		     viewBox="0 0 221 221" style="enable-background:new 0 0 221 221;" xml:space="preserve">
	 		 <path fill="#F8B317" d="M186.5,189v-81.863c0-33.075-20.935-61.307-50.379-71.96L152.934,0H67.476l17.589,34.888
	 		    C55.178,45.302,33.5,73.746,33.5,107.137V189h-10v32h174v-32H186.5z M130.025,154.81c-3.246,2.882-7.261,4.884-11.93,5.946
	 		    l0.078,10.615l-15.283,0.039l-0.069-10.619c-4.323-1.043-7.482-3.125-11.231-6.228c-5.107-4.401-7.767-9.543-7.938-15.311
	 		    l-0.003-0.116l0.382-4.872l14.708-0.052l0.115,3.101c0.057,3.663,1.111,6.311,3.222,8.141c2.138,1.918,3.981,2.881,8.196,2.881
	 		    c4.174-0.034,7.408-1.036,9.595-2.98c2.086-1.838,3.097-4.417,3.097-7.887c0-2.727-0.918-4.722-2.888-6.279
	 		    c-2.307-1.809-6.488-3.434-12.421-4.825c-7.31-1.701-12.923-4.348-16.682-7.868c-4.034-3.828-6.09-8.64-6.122-14.311
	 		    c-0.042-6.497,2.492-11.903,7.53-16.066c2.879-2.393,6.249-4.092,10.033-5.061l-0.071-9.301l15.279-0.106l0.039,8.974
	 		    c4.204,0.87,6.996,2.602,10.163,5.182c4.841,4.049,7.47,9.579,7.835,16.462l0.118,3.311l-14.771,0.073l-0.435-2.727
	 		    c-0.468-3.488-1.562-6.057-3.259-7.546c-1.796-1.575-3.052-2.38-6.181-2.38h-0.207c-3.646,0-6.548,0.989-8.415,2.581
	 		    c-1.811,1.515-2.601,3.593-2.578,6.422c0,2.327,0.774,4.087,2.366,5.396c1.33,1.12,4.193,2.733,10.392,4.156
	 		    c8.186,1.924,14.456,4.802,18.645,8.556c4.436,4.014,6.682,9.092,6.682,15.1C138.059,144.318,135.37,150.243,130.025,154.81z"/>
	 		 </svg>

			Cagnotte: <strong>CHF <?php echo $_SESSION["money"] ?>.-</strong>
			<?php if ( isset($bet) ) echo '<br>Mise: <strong>CHF ' . $bet . '.-</strong>' ?>
		</div>

		<?php
			switch ( $step )
			{
				case 1:
		?>
		<h2>Bienvenue à la table CREA !</h2>
		<form method="post">
			<input type="hidden" name="step" value="2" />
			<label for="bet">Votre mise:</label>
			<select name="bet" id="bet">
			<?php
				if ( $money >= 2 ) echo '<option value="2">CHF 2.-</option>';
				if ( $money >= 5 ) echo '<option value="5">CHF 5.-</option>';
				if ( $money >= 10 ) echo '<option value="10">CHF 10.-</option>';
				if ( $money >= 25 ) echo '<option value="25">CHF 25.-</option>';
				if ( $money >= 50 ) echo '<option value="50">CHF 50.-</option>';
				if ( $money >= 100 ) echo '<option value="100">CHF 100.-</option>';
				if ( $money >= 500 ) echo '<option value="500">CHF 500.-</option>';
			?>
				<option value="all">Totalité (tapis)</option>
			</select>
			<button type="submit">Valider</button>
		</form>
		<?php
				break;

				case 2:
		?>
		<div class="player">
			<h2>Croupier <span><?php echo $croupier_total ?></span></h2>
			<?php
				foreach ( $croupier_cards as $card )
					echo '<div class="card card-' . $card['color'] . '">' . $card['value'] . '</div>';

			?>
		</div>
		<div class="player">
			<h2>Joueur <span><?php echo $player_total ?></span></h2>
			<?php
				foreach ( $player_cards as $card )
					echo '<div class="card card-' . $card['color'] . '">' . $card['value'] . '</div>';

			?>
		</div>
		<div class="actions">
			<a href="#?param=carte">Nouvelle carte</a>
			<a href="#?param=passe">Passer son tour</a>
			<a href="#?param=mise">Doubler la mise !</a>
		</div>
		<?php
				break;
			}
		?>
	</div>

</body>
</html>
